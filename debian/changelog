pg-fact-loader (2.0.1-4) unstable; urgency=medium

  * Upload for PostgreSQL 17.
  * Restrict to 64-bit architectures.
  * Mark postgresql-all as <!nocheck>.

 -- Christoph Berg <myon@debian.org>  Tue, 17 Sep 2024 14:45:06 +0000

pg-fact-loader (2.0.1-3) unstable; urgency=medium

  * Build-depend on all postgresql-PGVERSIONS-pglogical-ticker.

 -- Christoph Berg <myon@debian.org>  Sat, 10 Feb 2024 11:27:28 +0100

pg-fact-loader (2.0.1-2) unstable; urgency=medium

  * Run tests at build time (needs postgresql-common 256).

 -- Christoph Berg <myon@debian.org>  Tue, 07 Nov 2023 16:16:34 +0000

pg-fact-loader (2.0.1-1) unstable; urgency=medium

  * New upstream version 2.0.1.
  * Upload for PostgreSQL 16.
  * Use ${postgresql:Depends}.
  * Use wal_level=logical for testing.
  * Move packaging repo to salsa.debian.org.

 -- Christoph Berg <myon@debian.org>  Mon, 16 Oct 2023 17:11:00 +0200

pg-fact-loader (1.7.0-3) unstable; urgency=medium

  * Disable unstable parts of test 17. (Closes: #1023226)

 -- Christoph Berg <myon@debian.org>  Tue, 01 Nov 2022 11:01:38 +0100

pg-fact-loader (1.7.0-2) unstable; urgency=medium

  * Upload for PostgreSQL 15.
  * Convert to dh --with pgxs.
  * R³: no.
  * debian/tests: Use 'make' instead of postgresql-server-dev-all.

 -- Christoph Berg <myon@debian.org>  Mon, 24 Oct 2022 20:30:11 +0200

pg-fact-loader (1.7.0-1) unstable; urgency=medium

  * Add pre-execute hook

 -- Jeremy Finzel <jfinzel@enova.com>  Tue, 23 Aug 2022 15:21:07 -0500

pg-fact-loader (1.6.0-2) UNRELEASED; urgency=medium

  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 05 Feb 2022 09:00:36 -0000

pg-fact-loader (1.6.0-1) unstable; urgency=medium

  * Change regproc to text to allow pgupgrade
  * Ensure pg12 readiness

 -- Jeremy Finzel <jfinzel@enova.com>  Mon, 31 Aug 2020 10:23:10 -0500

pg-fact-loader (1.5.2-1) unstable; urgency=medium

  * Fix time zone bug and behavior of SIGTERM

 -- Jeremy Finzel <jfinzel@enova.com>  Thu, 29 Nov 2018 10:36:36 -0600

pg-fact-loader (1.5.1-2) UNRELEASED; urgency=medium

  * Test-Depend on postgresql-contrib-PGVERSION.

 -- Christoph Berg <christoph.berg@credativ.de>  Wed, 28 Nov 2018 15:06:02 +0100

pg-fact-loader (1.5.1-1) unstable; urgency=medium

  * Fixes for old version tests

 -- Jeremy Finzel <jfinzel@enova.com>  Tue, 27 Nov 2018 09:23:57 -0600

pg-fact-loader (1.5.0-1) unstable; urgency=medium

  * Initial public version of pg_fact_loader

 -- Jeremy Finzel <jfinzel@enova.com>  Fri, 09 Nov 2018 11:54:11 -0600
